const glob = require('glob');

const logger = require('morgan');
const bodyParser = require('body-parser');
const {
    validationError,
    validationNotFoundService:notFoundMiddleware
} = require('../app/middleware/validationError');

module.exports = (app, config) => {
    const env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env == 'development';

    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    const controllers = glob.sync(config.root + '/app/routes/api/*.js');
    controllers.forEach((controller) => {
        require(controller)(app);
    });

    app.use(notFoundMiddleware);
    app.use(validationError);

    return app;
};
