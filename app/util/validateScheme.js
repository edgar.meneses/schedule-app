const Joi = require('@hapi/joi');

module.exports.schemaIsValid = (scheme, data) => {
   const hasError = Joi.validate(data, scheme);
   return hasError['error'];
};
