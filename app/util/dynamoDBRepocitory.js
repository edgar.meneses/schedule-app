const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const dynamondb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

module.exports.save = (tableName, items) => {
    return new Promise((resolve, reject) => {
        dynamondb.put(getSaveParams(tableName,items), (error, data) => {
            return (!error)?resolve(data):reject(error);
        })
    })
};

module.exports.fullScan = (tableName) => {
    return new Promise((resolve, reject) => {
        dynamondb.scan(getFullScanParams(tableName), (error, listData) => {
            return (!error)?resolve(listData['Items']):reject(error);
        });
    });
};

module.exports.delete = (tableName, keys) => {
    return new Promise((resolve, reject) => {
        dynamondb.delete(getDataParams(tableName, keys), (error, data) => {
            return (!error)?resolve(data):reject(error);
        });
    });
};

module.exports.get = (tableName, Key) => {
    return new Promise((resolve, reject) => {
        dynamondb.get(getDataParams(tableName, Key), (error, data) => {
            console.log(data);
            return (!error)?resolve(data['Item']):reject(error);
        });
    });
};

const getSaveParams = (tableName, items) => ({
    TableName: tableName,
    Item: items
});

const getFullScanParams = (tableName) => ({
    TableName: tableName
});

const getDataParams = (tableName, Key) =>({
    TableName: tableName,
    Key
});
