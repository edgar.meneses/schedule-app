const defaultCode = 200;
const errorCode = 500;

module.exports.sendResponse = (res, response)=> {
    res.status(response['code']||defaultCode);
    res.json(response);
};

module.exports.errorResponse = (message, trace, next) => {
    next({
        message: message,
        code: errorCode,
        trace: trace
    });
};
