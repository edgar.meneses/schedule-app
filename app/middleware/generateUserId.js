module.exports.generateId = (req, res, next)=>{
    const documentType =  req.body['documentType'];
    const documentNumber = req.body['documentNumber'];
    if( !!documentNumber && !!documentType) {
        req.body['id'] = `${documentType}-${documentNumber}`;
    }
    next();
};
