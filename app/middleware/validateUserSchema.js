const scheme = require('../components/user/models/user');
const {schemaIsValid} = require('../util/validateScheme');
const errorMessage = 'Los datos ingresados no son validos.';
const errorCode = 400;

module.exports = (req, res, next) => {
    const errorSchema = schemaIsValid(scheme, req.body);
    return (errorSchema)
        ? next({
            message: errorMessage,
            code: errorCode,
            trace: errorSchema
        })
        : next();
};
