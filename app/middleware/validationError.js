const {sendResponse} = require('../util/response');
const errorData = {
    error:{
        message:'Se presento un error al procesar la petición.',
        code: 500
    },
    notFound: {
        message: 'El servicio solicitado no esta disponible.',
        code: 404
    }
};

module.exports.validationError = (err, req, res, next) => {
    console.log(err);
    const {code, message} = errorData.error;
    sendResponse(res, {
        success: false,
        code: err.code || code,
        message: err.message || message
    })
};

module.exports.validationNotFoundService = (req, res, next) => {
    const {code, message} = errorData.notFound;
    next(
        {
            message: message,
            code: code
        }
    )
};
