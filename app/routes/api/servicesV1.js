// import express and router libs
const express = require('express');
const router = express.Router();
// import app services
const userServices = require('../../components/user/services');
const  classroomServices = require('../../components/classroom/services');
// declare constant and vars
const urlPathVersion = '/api/v1';
module.exports = app => {
    app.use(`${urlPathVersion}/user`, userServices);
    app.use(`${urlPathVersion}/classroom`, classroomServices);
    app.use(`${urlPathVersion}`, router);
};

router.get(`/health`, (req, res) => res.json(
    {
        success: true,
        code: 200,
        message: 'App online'
    }
));
