const Joi = require('@hapi/joi');
const {student, advisor} = require('./roles');
const {dni, foreignIdentityCard:card, passport} = require('./documentType');

const userSchema = Joi.object().keys({
    id:Joi.string().regex(/^[a-zA-Z0-9-]{3,50}$/).required(),
    email: Joi.string().email({ minDomainSegments: 2 }),
    name: Joi.string().regex(/^[a-zA-Z ]{3,50}$/).required(),
    role: Joi.string().valid(student, advisor).required(),
    documentNumber: Joi.string().alphanum().min(3).max(15).required(),
    documentType: Joi.string().valid(dni, card, passport).required(),
});

module.exports = userSchema;
