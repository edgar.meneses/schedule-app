const {
    save,
    fullScan,
    delete: deleteUser,
    get
} = require('../../../util/dynamoDBRepocitory');
const tableName = 'schedule-app-users';
module.exports.saveUser = async userData => {
    try {
        return await save(tableName, getUserItem(userData));
    } catch (e) {
        throw e;
    }
};

module.exports.getUsers = async () => {
    try {
        return await fullScan(tableName)
    }catch (e) {
        throw e;
    }
};

module.exports.deleteUser = async id => {
    try {
        return await deleteUser(tableName,{id});
    }catch (e) {
        throw e;
    }
};

module.exports.getUser = async id => {
    try {
        return await get(tableName,{id});
    }catch (e) {
        throw e;
    }
};

const getUserItem = userData => {
    const {id, email, name, role, documentNumber, documentType} = userData;
    return {
        id,
        email,
        name,
        role,
        documentType,
        documentNumber
    }
};
