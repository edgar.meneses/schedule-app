const {
    saveUser,
    getUsers,
    deleteUser,
    getUser,
} = require('../controllers/user');
const {
    sendResponse,
    errorResponse
} = require('../../../util/response');

module.exports.saveUser = async (req, res, next) => {
    try {
        await saveUser(req.body);
        sendResponse(res, {
            success: true,
            code: 201,
            message: 'Usuario guardado.'
        });
    } catch (e) {
        errorResponse('Error al guardar el usuario', e, next);
    }
};

module.exports.getUsers = async (req, res, next) => {
    try{
        const listUsers = await getUsers();
        sendResponse(res, {
            success: true,
            code: 200,
            message: 'Consulta exitosa.',
            data: listUsers
        });
    }catch (e) {
        errorResponse('Error al consultar la lista de usuarios', e, next);
    }
};

module.exports.deleteUser = async (req, res, next) => {
    try{
        await deleteUser(req.params['userId']);
        sendResponse(res, {
            success: true,
            code: 200,
            message: 'Se elimino correctamente.'
        });
    }catch (e) {
        errorResponse('Error al borrar el usuario', e, next);
    }
};

module.exports.getUser = async (req, res, next) => {
    try{
        const user = await getUser(req.params['userId']);
        sendResponse(res, {
            success: true,
            code: 200,
            message: 'Datos encontrados.',
            data: user
        });
    }catch (e) {
        errorResponse('Error al consultar el usuario', e, next);
    }
};



