// import express and router libs
const express = require('express');
const router = express.Router();
// import user services
const {
    saveUser,
    getUsers,
    deleteUser,
    getUser
} = require('./user');
// import app middleware
const schemeValidation = require('../../../middleware/validateUserSchema');
const {generateId} = require('../../../middleware/generateUserId');

// Lista de usuarios
router.get('/', getUsers);
// Guardar nuevo usuario
router.post('/',generateId, schemeValidation, saveUser);
// Eliminar usuario
router.delete('/:userId', deleteUser);
// Actualizar usuario
router.get('/:userId', getUser);
module.exports = router;
